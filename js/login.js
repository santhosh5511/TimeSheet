$(document).ready(function (){
	getData("sheet") == null ?putData("sheet",JSON.parse($("#datas").text())) : "";
    function getData(key){
		return JSON.parse(localStorage.getItem(key));
	}
	function putData(key , value){
		localStorage.setItem(key , JSON.stringify(value));
	} 

	var cc=chart();

					/*sign in function*/

	$(document).on('click',"#sign_btn",function(){
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var logged_username=$("#uname1").val();
		sessionStorage.setItem("key1", logged_username);//session used for getting id and name in user_interface page 
		var logged_password=$("#pass1").val() , i = 0;
		sessionStorage.setItem("key2", logged_password);
		var stype="";
				/*Time Functionalities and converison (below)*/
		var logintime = $.now(); 
		var c_in=new Date(logintime).toTimeString() ;
		var day1=c_in.split(" ");
		var cin=new Date(logintime).toUTCString() ;
		var timeStart = new Date(cin).getTime();
		sessionStorage.setItem("key10", logintime);
		for(var x in Slogin){  /*here checking current user and password*/
			if(stype!=""){
				break;
			}
			for(var y in Slogin[x]){
				if(Slogin[x][y].username==logged_username && Slogin[x][y].password==logged_password){
					stype=Slogin[x][y].type;
					sessionStorage.setItem("key3", stype);
						if(stype=="user"){
							var temp ={};
							temp["login_time"] = c_in;
							temp["logout_time"] = 0;
							temp["project"] = "";
							Slogin[x][y].login.push(temp);
							putData('sheet' , cuslogin);
							putData('current', Slogin[x][y]);
							window.location.href="user_interface.html";
							return true;
						}
						else{
							window.location.href="admin.html";
							return false;
						}
					break;
				}	
			}
		}
	});
				/*getting task*/
	$(document).on('change',"#role",function(){
		var role_name=$("#role").val();
		sessionStorage.setItem("key4",role_name);
		var flag=0;
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var logged_username = sessionStorage.getItem("key1");
		var logged_password = sessionStorage.getItem("key2");
		var stype=sessionStorage.getItem("key3");
		for(var x in Slogin){
			if(flag!=0){
				break;
			}
			for(var y in Slogin[x]){
				if(Slogin[x][y].username==logged_username && Slogin[x][y].password==logged_password){
					stype=Slogin[x][y].type;
					console.log(stype);
						if(stype=="user"){
							Slogin[x][y].login[Slogin[x][y].login.length-1]["project"] = role_name;
							putData('sheet' , cuslogin);
							flag=1;
							break;
						}
					}
				}
			}
	});
				/*for displaying current user using dynamic card */
	profile_display();
	function profile_display(){
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
	 	var logged_username = sessionStorage.getItem("key1");
	 	var logged_password = sessionStorage.getItem("key2");
	 	var stype=sessionStorage.getItem("key3");

	 	for(var x in Slogin){
	 		for(var y in Slogin[x]){
				if(Slogin[x][y].username==logged_username && Slogin[x][y].password==logged_password){
					var pro='<div class="card" style="width: 17rem; height:22rem;">'+
                    	'<img class="card-img-top" src="img/'+Slogin[x][y].image+'.jpg" alt="Card image cap">'+
	                        '<div class="card-body pt-0">'+
	                            '<p class="card-text">'+
	                                '<div class="row">'+
	                                    '<div class="col-md-6">'+
	                                    '<p>username:</p>'+
	                                    '</div>'+
	                                    '<div class="col-md-6 ">'+
	                                    '<p>'+Slogin[x][y].username+'</p>'+
	                                    '</div>'+
	                                '</div>'+
	                                '<div class="row pt-0">'+
	                                    '<div class="col-md-6">'+
	                                        '<p>Id:</p>'+
	                                    '</div>'+
	                                    '<div class="col-md-6">'+
	                                        '<p>'+Slogin[x][y].id+'</p>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</p>'+
	                        '</div>'+
	                	'</div>';
                $("#profile_display").append(pro);
				}
			}
		}
	}
				/*logout gets executed only after user selects id*/
	$(document).on('click',"#logout_btn",function(){
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var logouttime = $.now();
		var c_out=new Date(logouttime).toTimeString() ;
		var day=c_out.split(" ");
		var cout=new Date(logouttime).toUTCString();
		var timeEnd = new Date(cout).getTime();
		sessionStorage.setItem("key5", logouttime);
		var logged_username = sessionStorage.getItem("key1");
		var logged_password = sessionStorage.getItem("key2");
		var stype=sessionStorage.getItem("key3");
		var timeStart = sessionStorage.getItem("key10");
		var proj_name=sessionStorage.getItem("key4");
		var timediff=0;
		/*checking whether user selects task or not*/
		if(proj_name==null){
			alert("select your task");
			return ;
		}
		for(var x in Slogin){
			for(var y in Slogin[x]){
				if(Slogin[x][y].username==logged_username && Slogin[x][y].password==logged_password){
					stype=Slogin[x][y].type;
						if(stype=="user"){
							Slogin[x][y].login[Slogin[x][y].login.length-1]["logout_time"] = c_out;
							putData('sheet' , cuslogin);
							putData('current', Slogin[x][y]);
							var timediff = new Date(timeEnd - timeStart);
							var a = timediff / 1000; //in seconds
							var secDiff=a.toPrecision(4);
							var  b= timediff / 60 / 1000; //in minutes
							var minDiff=b.toPrecision(4);
							var  c= timediff / 3600 / 1000; //in hours
							var hDiff=c.toPrecision(2);
							var temp={};
							temp["hours"]=hDiff;
							temp["mins"]=minDiff;
							temp["secs"]=secDiff;
							Slogin[x][y].working_time.push(temp);

							var temp1={};
							if(temp1["thours"] == null){

								temp1["thours"] = hDiff;
								temp1["tmins"]  = minDiff;
								temp1["tsecs"]  = secDiff;
							}
							else{
								temp1["thours"] += hDiff;
								temp1["tmins"]  += minDiff;
								temp1["tsecs"]  += secDiff;
							}

							Slogin[x][y].Total_Time.push(temp1);
							putData('current', Slogin[x][y]);
							putData('sheet' , cuslogin);
							sessionStorage.clear();
							window.location.href="index.html";
						}
				}	
			}
		}
	});

							/*printing table data in admin*/
    $(document).on('click',"#table_btn",function(){
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
    	var table = getData("sheet").users[0]["user"];
    	var table1 = getData("sheet").users[0]["user"][0]["login"];
		var x=0,k=0;
    	var logout_time=$.now(); 
    	var j=0
      	var table_body = '<table border="1" id="example"><thead><tr><th>Id</th><th>Username</th><th>Mobile_number</th><th>Details</th></tr></thead><tbody>';
   
      	for(j in table){
            table_body +='<tr class="san">';

            table_body +='<td >';
            table_body +=table[j].id;
            table_body +='</td>';

            table_body +='<td >';
            table_body +=table[j].username;
            table_body +='</td>';
           
            table_body +='<td>';
            table_body +=table[j].Mobile_no;
            table_body +='</td>';

            table_body +='<td>';
            table_body +='<button type="button" class="btn btn-primary">view More</button>';
            table_body +='</td>';

            table_body+='</tr>';
      	}
       
	       table_body+='</tbody></table>';
	       $('#tableDiv').html(table_body);
		   putData('sheet' , cuslogin);

    });

            			/*table for particular profile */
    $(document).on('click','tr.san',function(){

		var this_id=($(this).text()[0]);
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var k=0;
		var table1=Slogin["user"];
      	var table_body = '<table border="1" id="example"><thead><tr><th>Id</th><th>Username</th><th>project</th><th>Starting_time</th><th>Ending_time</th></tr></thead><tbody>';
		var j=0;
      	for(var i in table1){
      		if(table1[i].id==this_id){
         		for(j in table1[i].login){
         			for(var k in table1[i].login[j]){

          			table_body +='<tr>';

          			table_body +='<td >';
                    table_body +=table1[i].id;
                    table_body +='</td>';

                 	table_body +='<td >';
                    table_body +=table1[i].username;
                    table_body +='</td>';

          			table_body +='<td >';
                    table_body +=table1[i].login[j].project;
                    table_body +='</td>';

                    table_body +='<td >';
                    table_body +=table1[i].login[j].login_time;
                    table_body +='</td>';

                    table_body +='<td >';
                    table_body +=table1[i].login[j].logout_time;
                    table_body +='</td>';

                	table_body+='</tr>';

          			break;
      			}
      		}
      	}
      }
			table_body+='</tbody></table>';
      		$('#Tableprofile').html(table_body);

    });
        		/*table for particluar user working time*/
    $(document).on('click','tr.san',function(){
		var this_id=($(this).text()[0]);
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var table1=Slogin["user"];
      	var table_body = '<table border="1" id="example"><thead><tr><th>Id</th><th>Username</th><th>Project</th><th>Hours</th><th>Minutes</th><th>Secs</th></tr></thead><tbody>';
		for(var i in table1){
      		if(table1[i].id==this_id){
      			for(j in table1[i].working_time){
      				for(var k in table1[i].working_time[j]){
      		
						table_body +='<tr>';

						table_body +='<td >';
	                    table_body +=table1[i].id;
	                    table_body +='</td>';

	                    table_body +='<td >';
	                    table_body +=table1[i].username;
	                    table_body +='</td>';

	                    table_body +='<td >';
                    	table_body +=table1[i].login[j].project;
                    	table_body +='</td>';

	                    table_body +='<td >';
	                    table_body +=table1[i].working_time[j].hours;
	                    table_body +='</td>';

	                    table_body +='<td >';
	                    table_body +=table1[i].working_time[j].mins;
	                    table_body +='</td>';

	                    table_body +='<td >';
	                    table_body +=table1[i].working_time[j].secs;
	                    table_body +='</td>';

	                    table_body+='</tr>';

              			break;
          			}
          		}
          	}
     	}
  			table_body+='</tbody></table>';
      		$('#Timeprofile').html(table_body);

	});

        			/*search will done for whole table */
    $("#search").on("keyup", function(){
    	var value = $(this).val().toLowerCase();
    		$("table tr").filter(function(index) {
      			if(index>0){
  					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				}
			});
	});
	        		/*adding new employee*/
  	$(document).on('click',"#new_btn",function(){
		var cuslogin = getData("sheet") , Slogin = cuslogin.users[0];
		var new_user=$("#newuser").val();
		var new_password=$("#newuser_Password").val();
		var new_id=$("#newuser_id").val();
		var new_mobile=$("#newuser_mob").val();
		var new_mail=$("newuser_mail").val();
		var temp={};
		temp["id"]=new_id;
		temp["username"]=new_user;
		temp["password"]=new_password;
		temp["Mobile_no"]=new_mobile;
		temp["type"]="user";
		temp["Email_Id"]="new_mail";
		temp["login"]=[];
        temp["Total_Time"]=[];
		Slogin.push(temp);				
		putData('sheet' , cuslogin);
	});

			/*admin logout button returning to home page*/
  	$(document).on('click',"#admin_logout",function(){
		window.location.href="index.html";
		return true;
	});

/*chart function*/
function chart(){
	try {
		
    var ctx = document.getElementById("sales-chart");
    if (ctx) {
      ctx.height = 150;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Anto", "Selva","Satish","Yeshu"],
          type: 'line',
          defaultFontFamily: 'Poppins',
          datasets: [{
            label: "TASK A",
            data: [1,2,3,6],
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
          }, {
            label: "TASK B",
            data: [1,2,7,6],
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Poppins',
            bodyFontFamily: 'Poppins',
            cornerRadius: 3,
            intersect: false,
          },
          legend: {
            display: false,
            labels: {
              usePointStyle: true,
              fontFamily: 'Poppins',
            },
          },
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'EMPLOYEE NAME'
              },
              ticks: {
                fontFamily: "Poppins"
              }
            }],
            yAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'TOTAL WORK IN TASKA AND TASKB',
                fontFamily: "Poppins"

              },
              ticks: {
                fontFamily: "Poppins",
                // beginAtZero: true,
                // stepSize: 1
              }
            }]
          },
          title: {
            display: false,
            text: 'Normal Legend'
          }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
}

	var logintime = $.now(); 
	var date=new Date(logintime).toUTCString() 
	var c_in=new Date(logintime).toTimeString() ;
	var day=new Date(date).getUTCDay();
	var timeStart = new Date(c_in).getTime();
	if(day==0 || day==6){
		alert("today is holiday");
		document.getElementById("log").disabled = true;
	}

});
